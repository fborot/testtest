const os = require('os');
const si = require('systeminformation');

class MachineInfo {
    static async getHeartbeat(_processes) {
      let details = {};
      
      details.hostname = hostname();
      details.interfaces = interfaces();
      details.cpuload = await cpu();
      details.cputemperature = await cputemp();
      details.memory = memory();
      details.uptime_minutes = uptime();
      details.processes = await processes(_processes);
      details.osInfo = await osInfo();
      details.usersInfo = await usersInfo();
      details.systemInfo = await systemInfo();
      details.biosInfo = await biosInfo();
      details.baseboardInfo = await baseboardInfo();
      details.cpuInfo = await cpuInfo();

      return new this(details);
    }

    constructor(_results) {
      this.results = _results;
    }

    result() {
      return this.results;
    }
  };

  async function cpu() {
    try {
      const data = await si.currentLoad();
      return data.currentload.toFixed(2);
    } catch (e) {
      return ""
    }
  }

  async function cputemp() {
    try {
      const data = await si.cpuTemperature();
      return JSON.stringify(data);
    } catch (e) {
      return ""
    }
  }

  async function processes(list) {
      let data = [];
      for(let x = 0; x < list.length; x++) {
        try {
            const _data = await si.services(list[x]);
            
            _data.forEach(function (_service) {
              data.push(_service);
            })
          } catch (e) {
        }
      }
      return data; 
  }

  function interfaces() {
    let ifaces = os.networkInterfaces();
    let details = [];
    Object.keys(ifaces).forEach(function (ifname) {
        ifaces[ifname].forEach(function (iface) {
            if ('IPv4' === iface.family && iface.internal === false) {
              details.push(
                {
                  Name: ifname,
                  Address: iface.address
                }
              );
            }
        });
    });

    return details;
  }

  function hostname() {
    return os.hostname();
  }

  function uptime() {
    return (os.uptime()/60).toFixed(2);
  }

  function memory() {
    return {
      memory_gb: (os.totalmem() / 1073741824).toFixed(2),
      memory_available_gb : (os.freemem() / 1073741824).toFixed(2)
    };
  }

  function osInfo(){
    return si.osInfo();
  }

  function usersInfo(){
    return si.users();
  }

  function systemInfo(){
    return si.system();
  }

  function biosInfo() {
    return si.bios();
  }

  function baseboardInfo() {
    return si.baseboard();
  }

  function cpuInfo() {
    return si.cpu();
  }

async function main() {
    _processes = [
      'z-way-server'
    ];
	setInterval(async function(process) {
	    let data = await MachineInfo.getHeartbeat(_processes);
	    console.log(data.result());
	}, 2000);
}

main();